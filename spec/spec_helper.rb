require 'pry'
require 'rspec/core'

DIR_LIB = "./lib/**{,/*/**}/*.rb"

Dir[DIR_LIB].each do |file|
  require file
end

RSpec.configure do |config|
end
