require_relative './spec_helper'

describe LogLine do
  describe ".parse" do
    let(:path) { "/recipe/777" }
    let(:ip_address) { "192.168.1.1"}
    let(:line) { "#{path} #{ip_address}" }

    subject(:result) { LogLine::SimpleLogLine.parse(line) }

    it { expect(result.path).to eq(path) }
    it { expect(result.ip_address).to eq(ip_address) }
  end
end
