require_relative './spec_helper'

describe LogFile do

  describe ".parse" do
    let(:file_path) { 'log/webserver.log' }

    subject(:log_file) { LogFile.parse(file_path, :simple) }

    it "successfully parses log file" do
      expect(
        log_file.log_lines.count
      ).to eq(
        Parser.parse(LogFile.new(file_path, :simple)).count
      )
    end
  end
end
