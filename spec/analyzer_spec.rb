require_relative './spec_helper'

describe Analyzer do
  let(:log_lines) do
    [].tap { |log_lines|
      log_lines << LogLine::SimpleLogLine.parse('/recipes/777 192.168.1.1')
      log_lines << LogLine::SimpleLogLine.parse('/recipes/777 127.0.0.1')
      log_lines << LogLine::SimpleLogLine.parse('/recipes/888 127.0.0.1')
    }
  end
  let(:analyzer) { Analyzer.new('path_to_log.log', :simple) }

  before { allow(Parser).to receive(:parse).and_return(log_lines) }

  describe "#print_page_views" do
    subject { analyzer.print_page_views }

    it "prints page views by path order by page views" do
      expect{
        subject
      }.to output(
%Q|/recipes/777 2 visits
/recipes/888 1 visits
|
      ).to_stdout
    end
  end

  describe "#print_unique_views" do

    subject { analyzer.print_unique_views }

    it "prints unique views by ip address order by page views" do
      expect{
        subject
      }.to output(
%Q|/recipes/777 2 unque views
/recipes/888 1 unque views
|
      ).to_stdout
    end
  end
end
