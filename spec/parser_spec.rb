require_relative './spec_helper'

describe Parser do
  describe ".parse" do
    context "when file exists" do
      let(:log_file) { LogFile.new('log/webserver.log', :simple) }

      subject(:log_lines) { Parser.parse(log_file) }

      it "successfully parses log file" do
        expect(log_lines.count).to be > 1
      end
    end

    context "when file doesn't exist" do
      let(:log_file) { LogFile.new('log/foo.log', :simple) }

      subject(:parsing) { Parser.parse(log_file) }

      it { expect{ parsing }.to raise_error }
    end
  end
end
