# Abstract parser for log files
class Parser

  # Parses a specified log file
  # == Parameters:
  #   An instance of LogFile
  #
  # == Returns:
  #   Array of LogLine
  def self.parse(log_file)
    File.open(log_file.file_path, "r") do |file|
      @lines = []
      file.each_line do |line|
        @lines << log_file.log_line_class.parse(line)
      end
      @lines
    end
  end
end
