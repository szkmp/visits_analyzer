# Represents single line of a log file
class LogLine

  # Parses single line of a log file
  # == Parameters:
  #   line::
  #     example.
  #       '/contact 543.910.244.929'
  #
  # == Returns:
  #   An instance of LogLine
  def self.parse(line)
    columns = line.split(separator)
    self.new(*columns)
  end
end
