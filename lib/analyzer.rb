# Convenient methods for analyzing log files
#
# == Usage
#   analyzer = Analyzer.new(file_path, type = :simple)
#   analyzer.print_page_views
#   analyzer.print_unque_views
class Analyzer
  def initialize(file_path, type = :simple)
    @log_file = LogFile.parse(file_path, type)
  end

  # Prints page views by path ordered by page views in desc
  #
  # == Outputs:
  #  example
  #  ```
  #  /about/2 90 visits
  #  /contact 89 visits
  #  /help_page/1 80 visits
  #  ```
  def print_page_views
    path_data = sorted(group_by_path) {|path_data|
                  path_data.sort_by{|path, value| -value[:page_views] }
                }
    path_data.each do |path, data|
      puts "#{path} #{data[:page_views]} visits"
    end
  end

  # Prints unique views by path ordered by unique views in desc
  #
  # == Outputs:
  #  example
  #  ```
  #  /about/2 90 unque views
  #  /contact 89 unque views
  #  /index 82 unque views
  #  ```
  def print_unique_views
    path_data = sorted(group_by_path_and_ip_address) {|path_data|
                  path_data.sort_by{|path, value| -value[:ip_addresses].count }
                }
    path_data.each do |ip_address, data|
      puts "#{ip_address} #{data[:ip_addresses].count} unque views"
    end
  end

  private

  def group_by_path
    @log_file.log_lines.each_with_object({}) {|log_line, log_lines|
      path = (log_lines[log_line.path] ||= { page_views: 0 })
      path[:page_views] += 1
    }
  end

  def group_by_path_and_ip_address
    @log_file.log_lines.each_with_object({}) {|log_line, log_lines|
      path = (log_lines[log_line.path] ||= {ip_addresses: []})
      (path[:ip_addresses] << log_line.ip_address).uniq
    }
  end

  def sorted(key_value)
    sorted = yield(key_value)
    # NOTE: This converts array to hash
    sorted.each_with_object({}) { |row, sum|
      sum[row[0]] = row[1]
    }
  end
end
