require_relative '../log_line'

class LogLine::SimpleLogLine < LogLine
  attr_accessor :path, :ip_address

  SEPARATOR = ' '.freeze

  def initialize(path, ip_address)
    @path = path
    @ip_address = ip_address
  end

  private

  def self.separator
    SEPARATOR
  end
end
