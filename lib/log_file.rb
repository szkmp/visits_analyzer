# Represents single log file
#
class LogFile
  attr_reader :file_path
  attr_accessor :log_lines

  def initialize(file_path, type)
    @file_path = file_path
    @type = type
  end

  # Parses and generates an instance of LogFile from the result
  #
  # == Parameters:
  # file_path::
  #   Path to log file to be loaded
  #
  # type::
  #   [optional] Type of format of single LogLine
  #   :simple means line consists of space separated values
  def self.parse(file_path, type = :simple)
    self.new(file_path, type).tap {|log_file|
      log_file.log_lines = Parser.parse(log_file)
    }
  end

  def log_line_class
    case @type
    when :simple
      LogLine::SimpleLogLine
    end
  end
end
