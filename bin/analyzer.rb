#!/usr/bin/env ruby

require 'pry'
require 'optparse'

DIR_LIB = "./lib/**{,/*/**}/*.rb"

Dir[DIR_LIB].each do |file|
  require file
end

params = ARGV.getopts('', 'type:')
file_path = ARGV[0]
if params["type"]
  analyzer = Analyzer.new(file_path, params["type"].to_sym)
else
  analyzer = Analyzer.new(file_path)
end

puts "PAGE_VIEWS:"
analyzer.print_page_views

puts ''
puts "UNIQUE_VIEWS:"
analyzer.print_unique_views
