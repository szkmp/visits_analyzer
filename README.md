This is code challenge for Arena Flowers


# Visits Analyzer

Visits Analyzer is my response for quiz on https://gitlab.com/szkmp/visits_analyzer/blob/master/problem/README.txt, which analyses log of a webserver.

## Usage

### Command Line
```
PROJECT_ROOT% bin/analyzer.rb log/webserver.log
```

### Output
```
/about/2 90 visits
/contact 89 visits
/help_page/1 80 visits
:
/about/2 90 unque views
/contact 89 unque views
/index 82 unque views
:
```

### Options

#### --type
Option to specify type of log file. Although it acceppts only `--type simple` for space separated values for the time being, this is designed to get format of log files replaceable as a future expansion.


## Spec
To test all lib files at a time, run the following rake command.
```
% bundle exec rake spec
```
